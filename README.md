Arduino sketches and micro:bit hex files used in microna.

Requires FastLED library.



Micro:bit pins:

Pins 0 and 1: Motor connected to a conveyor belt.

Pin 5: Button to start the game.

Pin 7: Servo. (Not working properly)

Pin 10: Input from connected steering wheel.

Pin 12: LED indicator for loss of lives

Pins 8 and 15: Serial.