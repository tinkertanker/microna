#include<FastLED.h>
#define NUM_LEDS 80
#define DATA_PIN 2

CRGBArray<NUM_LEDS> leds;

int rows = 10;
int cols = 8;
int screen [10][8][3]; //rows-cols-rgb
int remaining [8]; //rows, meant to indicate how many more in the first row
int number = 0;
int countdown = 5;
int carCurr = 0;

//===============================================================================================================================

void nextObs(int pos, int type) {
  // Updates the led lights to display obstacle positions/car position
  int carType[4][4]  = {
    {2, 0, 0, 255},     //Type 1: normal
    {3, 0, 255, 0},     //Type 2: truck
    {1, 255, 255, 0},   //Type 3: coin
    {1, 255, 0, 0}      //Type 4: extra life
  };

  int obsType = type - 1;
  int obsPos = pos - 1;
  char obsRed = carType[obsType][1];
  char obsGreen = carType[obsType][2];
  char obsBlue = carType[obsType][3];

  for(int r = rows - 2; r > -1; r--) {
    for(int c = 0; c < cols; c++) {
      int red = screen[r][c][0];
      int green = screen[r][c][1];
      int blue = screen[r][c][2];
      screen[r+1][c][0] = red;
      screen[r+1][c][1] = green;
      screen[r+1][c][2] = blue;
      if (r % 2 == 0) {
        leds[(r + 2) * cols - c - 1].setRGB(red,green,blue);
      } else {
        leds[(r + 1) * cols + c].setRGB(red,green,blue);
      }
    }
  }
  for (int c=0; c<cols; c++) {
    if (obsType == 1) {
      if (c == obsPos || (c == obsPos + 1 && pos != 0)) {
        leds[c].setRGB(obsRed,obsGreen,obsBlue);
        screen[0][c][0] = obsRed;
        screen[0][c][1] = obsGreen;
        screen[0][c][2] = obsBlue;
        remaining[c] = carType[obsType][0] - 1;
      } else {
        if (remaining[c] == 0) {
          leds[c].setRGB(0,0,0);
          screen[0][c][0] = 0;
          screen[0][c][1] = 0;
          screen[0][c][2] = 0;
        } else {
          int red = screen[0][c][0];
          int green = screen[0][c][1];
          int blue = screen[0][c][2];
          leds[c].setRGB(red,green,blue);
          remaining[c] -= 1;
        }
      }
    } else {
      if (c == obsPos) {
        leds[c].setRGB(obsRed,obsGreen,obsBlue);
        screen[0][c][0] = obsRed;
        screen[0][c][1] = obsGreen;
        screen[0][c][2] = obsBlue;
        remaining[c] = carType[obsType][0] - 1;
      } else {
        if (remaining[c] == 0) {
          leds[c].setRGB(0,0,0);
          screen[0][c][0] = 0;
          screen[0][c][1] = 0;
          screen[0][c][2] = 0;
        } else {
          int red = screen[0][c][0];
          int green = screen[0][c][1];
          int blue = screen[0][c][2];
          leds[c].setRGB(red,green,blue);
          remaining[c] -= 1;
        }
      }
    }
  }
  for (int r = rows - 2; r < rows; r++) {
    // Car position
    for (int c = 0; c < cols; c++) {
      if (c == carCurr) {
        if (r % 2 == 0) {
          leds[r * cols + c].setRGB(255,255,255);
        } else {
          leds[(r + 1) * cols - c - 1].setRGB(255,255,255);
        }
      }
    }
  }
}
//===============================================================================================================================

void checkSpeed() {
  // Receives information from the microbit
  // If incomplete information is received, assume no new obstacles generated
  boolean reset = false;
  boolean started = false;
  char carPos = -1;
  char pos = -1;
  char type = -1;
  char scoring = -1;
  char life = -1;
  boolean ended = false;

  if (Serial.available()) {
    while (true) {
      if (Serial.available()) {    
        char c = Serial.read();
        Serial.write(c);
        switch (c) {
          case '#':
            reset = true;
          case '(':
            countdown = 5;
            started = true;
            carPos = -1;
            pos = -1;
            type = -1;
            scoring = -1;
            life = -1;
            ended = false;
            break;  
          case ')':
            if (life != -1) {
              ended = true;
              break;
            } else {
              ended = true;
              started = false;
              pos = 0;
              type = 0;
              scoring = 1;
              life = 0;
              break;
            }    
            break;
          default:
            if (life != -1) {
               started = false;
               pos = -1;
               type = -1;
               scoring = -1;
               life = -1;
            } else if (scoring != -1) {
              life = c;
            } else if (type != -1) {
              scoring = c;
            } else if (pos != -1) {
              type = c;
            } else if (carPos != -1) {
              pos = c;
            } else if (started) {
              carPos = c;
            }
        }
      }
      if (reset) {
        // Shows a countdown on lives as to when the game starts
        number = 0;
        showNumber(countdown);
        countdown--;
        FastLED.delay(20);
        break;
      }
      else if (ended) {
        nextObs(pos - '0', type - '0');
        if (scoring == '2') {
          number += 11;
        } else {
          number += 1;
        }
        carCurr = carPos - '0' - 1;
        int display_score = floor(number/10);
        int display = display_score * 10 + life - '0';
        showNumber(display);
//        showNumberLives(life);
        FastLED.delay(20);
        break;
      }
    }
  }

}


    /*
 Controlling large 7-segment displays
 By: Nathan Seidle
 SparkFun Electronics
 Date: February 25th, 2015
 License: This code is public domain but you buy me a beer if you use this and we meet someday (Beerware license).

 This code demonstrates how to post two numbers to a 2-digit display usings two large digit driver boards.

 Here's how to hook up the Arduino pins to the Large Digit Driver IN

 Arduino pin 6 -> CLK (Green on the 6-pin cable)
 5 -> LAT (Blue)
 7 -> SER on the IN side (Yellow)
 5V -> 5V (Orange)
 Power Arduino with 12V and connect to Vin -> 12V (Red)
 GND -> GND (Black)

 There are two connectors on the Large Digit Driver. 'IN' is the input side that should be connected to
 your microcontroller (the Arduino). 'OUT' is the output side that should be connected to the 'IN' of addtional
 digits.

 Each display will use about 150mA with all segments and decimal point on.

*/

//GPIO declarations
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
byte segmentClock = 6;
byte segmentLatch = 5;
byte segmentData = 7;

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=

void showNumber(float value)
{
  int number = abs(value); //Remove negative signs and any decimals
  for (byte x = 0 ; x < 4 ; x++)
  {
    int remainder = number % 10;
    postNumber(remainder, false);
    number /= 10;
  }
  //Latch the current segment data
  digitalWrite(segmentLatch, LOW);
  digitalWrite(segmentLatch, HIGH); //Register moves storage register on the rising edge of RCK
}

//Given a number, or '-', shifts it out to the display
void postNumber(byte number, boolean decimal)
{
  //    -  A
  //   / / F/B
  //    -  G
  //   / / E/C
  //    -. D/DP

#define a  1<<0
#define b  1<<6
#define c  1<<5
#define d  1<<4
#define e  1<<3
#define f  1<<1
#define g  1<<2
#define dp 1<<7

  byte segments;

  switch (number)
  {
    case 1: segments = b | c; break;
    case 2: segments = a | b | d | e | g; break;
    case 3: segments = a | b | c | d | g; break;
    case 4: segments = f | g | b | c; break;
    case 5: segments = a | f | g | c | d; break;
    case 6: segments = a | f | g | e | c | d; break;
    case 7: segments = a | b | c; break;
    case 8: segments = a | b | c | d | e | f | g; break;
    case 9: segments = a | b | c | d | f | g; break;
    case 0: segments = a | b | c | d | e | f; break;
    case ' ': segments = 0; break;
    case 'c': segments = g | e | d; break;
    case '-': segments = g; break;
  }

  if (decimal) segments |= dp;

  shiftOut(segmentData, segmentClock, MSBFIRST, segments);
}

//void showNumberLives(float value)
//{
//  int number = abs(value); //Remove negative signs and any decimals
//
//  postNumberLives(number, false);
//
//  //Latch the current segment data
//  digitalWrite(segmentLatchLives, LOW);
//  digitalWrite(segmentLatchLives, HIGH); //Register moves storage register on the rising edge of RCK
//}
//
////Given a number, or '-', shifts it out to the display
//void postNumberLives(byte number, boolean decimal)
//{
//  //    -  A
//  //   / / F/B
//  //    -  G
//  //   / / E/C
//  //    -. D/DP
//
//  byte segments;
//
//  switch (number)
//  {
//    case 1: segments = b | c; break;
//    case 2: segments = a | b | d | e | g; break;
//    case 3: segments = a | b | c | d | g; break;
//    case 4: segments = f | g | b | c; break;
//    case 5: segments = a | f | g | c | d; break;
//    case 6: segments = a | f | g | e | c | d; break;
//    case 7: segments = a | b | c; break;
//    case 8: segments = a | b | c | d | e | f | g; break;
//    case 9: segments = a | b | c | d | f | g; break;
//    case 0: segments = a | b | c | d | e | f; break;
//    case ' ': segments = 0; break;
//    case 'c': segments = g | e | d; break;
//    case '-': segments = g; break;
//  }
//
//  if (decimal) segments |= dp;
//
//  shiftOut(segmentDataLives, segmentClockLives, MSBFIRST, segments);
//}

//===============================================================================================================================

void setup() {
  pinMode(segmentClock, OUTPUT);
  pinMode(segmentData, OUTPUT);
  pinMode(segmentLatch, OUTPUT);
  
//  pinMode(segmentClockLives, OUTPUT);
//  pinMode(segmentDataLives, OUTPUT);
//  pinMode(segmentLatchLives, OUTPUT);

  digitalWrite(segmentClock, LOW);
  digitalWrite(segmentData, LOW);
  digitalWrite(segmentLatch, LOW);
  
//  digitalWrite(segmentClockLives, LOW);
//  digitalWrite(segmentDataLives, LOW);
//  digitalWrite(segmentLatchLives, LOW);
  
  FastLED.addLeds<NEOPIXEL,DATA_PIN>(leds, NUM_LEDS);

  showNumber(0009);
//  showNumberLives(3);
  Serial.begin(115200);
  
}

//===============================================================================================================================

void loop() {
  // put your main code here, to run repeatedly:
  checkSpeed();
 
//   for testing purposes
//  nextObs(1,2);
//  FastLED.delay(500);
//    nextObs(0,0);
//  FastLED.delay(500);
//  nextObs(1,0);
//  FastLED.delay(500);
//  nextObs(2,1);
//  FastLED.delay(500);
//  nextObs(2,0);
//  FastLED.delay(500);
//  nextObs(4,3);
//  FastLED.delay(500);
  
}
